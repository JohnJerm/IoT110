$(document).ready(function() {

    var max_data_saved = 20;
    //Add variable to hold data between receive data events
    var sse_sensor_data = [];

    // the key event receiver function
    iotSource.onmessage = function(e) {
        // parse sse received data
        parsed_json_data = JSON.parse(e.data);

        // For now just log to console
        // console.log(parsed_json_data);

        // Convert string datetime to JS Date object
        parsed_json_data['meas_time'] = new Date(parsed_json_data['meas_time']);
        console.log(parsed_json_data);

        // Push new data to front of saved data list
        sse_sensor_data.unshift(parsed_json_data);

        // Then remove oldest data up to maximum data saved
        while (sse_sensor_data.length > max_data_saved) { sse_sensor_data.pop(); }

        updateEnvironmentalTable();
        update_env_chart();
        updateInertialTable();
        update_accel_chart();
		
		//updateInertialChartData(parsed_json_data);

        // update Imu Chart Data
        // update Joystick Data
        // update Dislay Data
    }

    // ============================ DATE FUNCTIONS ==============================
    // from http://stackoverflow.com/questions/2998784/how-to-output-integers-with-leading-zeros-in-javascript
    function zeropad(num, size) {
        var s = "000000000" + num;
        return s.substr(s.length - size);
    }

    function getDateNow() {
        var d = new Date();
        var date = (d.getFullYear()) + '-' + d.getMonth() + 1 + '-' + d.getDate();
        var time = zeropad(d.getHours(), 2) + ':' + zeropad(d.getMinutes(), 2) +
            ':' + zeropad(d.getSeconds(), 2) + '.' + zeropad(d.getMilliseconds(), 3);
        return { time: time, date: (date + " " + time) };
    }

    // ============================ DATE FUNCTIONS ==============================

    // ============================== ENV TABLE =================================
    function updateEnvironmentalTable() {
        $('tr.env-param-row').each(function(i) {
            var new_html = '<td>' + sse_sensor_data[i].meas_time.toLocaleString() + '</td>';
            new_html += '<td>' + sse_sensor_data[i]['environmental']['temperature'].value.toFixed(2) + '</td>';
            new_html += '<td>' + sse_sensor_data[i]['environmental']['pressure'].value.toFixed(2) + '</td>';
            new_html += '<td>' + sse_sensor_data[i]['environmental']['humidity'].value.toFixed(2) + '</td>';
            $(this).html(new_html);
        });
    }
    // ============================== ENV TABLE =================================

    // ============================ INERTIAL TABLE ==============================
    function updateInertialTable() {
        console.log("updateInertialTable");		
        $('tr.imu-param-row').each(function(i) {
            var new_html = '<td>' + sse_sensor_data[i].meas_time.toLocaleString() + '</td>';
            new_html += '<td>' + sse_sensor_data[i]['inertial']['accelerometer']['x'].toFixed(2) + '</td>';
            new_html += '<td>' + sse_sensor_data[i]['inertial']['accelerometer']['y'].toFixed(2) + '</td>';
            new_html += '<td>' + sse_sensor_data[i]['inertial']['accelerometer']['z'].toFixed(2) + '</td>';
            
            new_html += '<td>' + sse_sensor_data[i]['inertial']['orientation']['pitch'].toFixed(2) + '</td>';
            new_html += '<td>' + sse_sensor_data[i]['inertial']['orientation']['roll'].toFixed(2) + '</td>';
            new_html += '<td>' + sse_sensor_data[i]['inertial']['orientation']['yaw'].toFixed(2) + '</td>';
			
            new_html += '<td>' + sse_sensor_data[i]['inertial']['orientation']['compass'].toFixed(2) + '</td>';
            console.log(new_html);
            $(this).html(new_html);	
        });
    }
    // ============================ Clear Table Rows ==============================

    function clearTableRow(tableRowName) {
        $('tr#' + tableRowName).each(function(i) {
            $(this).empty();
        });
    }

    // ============================== ENV CHART ================================
    // initialize the accel chart structure
    var env_chart = new Morris.Line({
        element: 'env-chart',
        data: '',
        xkey: 'time',
        ykeys: ['humidity', 'temp'],
        labels: ['%RH', '&degC']
    });

    // build the environmental chart data array for MorrisJS structure
    function update_env_chart(data) {
        var chart_data = [];
        sse_sensor_data.forEach(function(d) {
            env_record = {
                'time': d['meas_time'].getTime(),
                'humidity': d['environmental']['humidity'].value,
                'temp': d['environmental']['temperature'].value
            };
            //console.log(env_record);
            chart_data.push(env_record);
        });
        //console.log(chart_data);
        env_chart.setData(chart_data);
    };
    // ============================== ENV CHART ================================

    // ============================== ACCEL CHART ================================
    // initialize the accel chart structure
    var accel_chart = new Morris.Line({
        element: 'accel-chart',
        data: [],
        xkey: 'time',
        ykeys: ['x', 'y', 'z'],
        labels: ['Accel-X', 'Accel-Y', 'Accel-Z']
    });

    // build the chart data array for MorrisJS structure
    function update_accel_chart(data) {
        var chart_data = [];
        // var i = 0;
        sse_sensor_data.forEach(function(d) {
            accel_record = {
                //time: d['date'].getTime(),
                'time': d['meas_time'].getTime(),
                'x': d['inertial']['accelerometer']['x'],
                'y': d['inertial']['accelerometer']['y'],
                'z': d['inertial']['accelerometer']['z']
            };
            chart_data.push(accel_record);
        });
        accel_chart.setData(chart_data);
    };
    // ============================== ACCEL CHART ================================
});